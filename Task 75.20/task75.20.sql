--1 Hiển thị toàn bộ thông tin của tất cả sinh viên 
SELECT * FROM students;

--2 Hiển thị mã sinh viên và username của tất cả sinh viên 
SELECT student_code, user_name FROM students;

--3 Hiển thị toàn bộ thông tin môn học, mà có credit (số tin chỉ) > 3 
SELECT * FROM subjects WHERE credit > 3;

--4 Hiển thị toàn bộ thông tin điểm có ngày thi (exam_date) trước ngày 01/05/2021 
SELECT * FROM grades WHERE exam_date < "2021-05-01";

--5 Hiển thị thông tin bảng điểm có các thông tin: student_code, subject_id, grade và exam_date 
SELECT s.student_code, g.subject_id, g.exam_date, g.grade FROM students AS s INNER JOIN grades AS g ON s.id = g.student_id;

--6 Hiển thị thông tin bảng điểm có các thông tin: student_code, fullname, subject_id, grade và exam_date của môn có subject_id = 1
SELECT s.student_code, g.subject_id, g.exam_date, g.grade, CONCAT(s.first_name, " ", s.last_name) AS fullname FROM students AS s INNER JOIN grades AS g ON s.id = g.student_id WHERE g.subject_id = 1;

--7 Hiển thị toàn bộ thông tin bảng điểm: Tên môn, mã sinh viên, họ và tên, điểm, ngày thi
SELECT 
	sj.subject_name,
    s.student_code,
    CONCAT(s.first_name, " ", s.last_name) AS fullname,
    g.grade,
    g.exam_date
FROM
	students AS s
    INNER JOIN grades AS g ON s.id = g.student_id
    INNER JOIN subjects AS sj ON sj.id = g.subject_id;

--8 Tổng hợp xem mỗi môn có bao nhiêu học viên đã thi: lấy ra subject_id và số lượng tương ứng
SELECT subject_id, COUNT(student_id) FROM grades GROUP BY subject_id;

--9 Tổng hợp ra các môn có số lượng học viên đã thi > 5. Lấy ra subject_id và số lượng tương ứng 
SELECT subject_id, COUNT(student_id) FROM grades GROUP BY subject_id HAVING COUNT(student_id) > 5;

--10 Tổng hợp ra các môn có số lượng học viên đã thi > 5. Lấy ra tên môn và số lượng tương ứng
SELECT s.subject_name, COUNT(g.student_id) FROM grades AS g JOIN subjects AS s ON s.id = g.subject_id GROUP BY g.subject_id HAVING COUNT(g.student_id) > 5;

--11 Lấy ra thông tin 3 điểm thi cao nhất: môn học, tên sinh viên, điểm thi, ngày thi 
SELECT sj.subject_name, CONCAT(s.first_name, " ", s.last_name) AS fullname, g.grade, g.exam_date FROM students AS s INNER JOIN grades AS g ON s.id = g.student_id INNER JOIN subjects AS sj ON sj.id = g.subject_id ORDER BY g.grade DESC LIMIT 3;