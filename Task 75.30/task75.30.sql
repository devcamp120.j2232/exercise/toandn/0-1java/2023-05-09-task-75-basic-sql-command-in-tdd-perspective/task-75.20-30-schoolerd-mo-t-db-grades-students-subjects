--1 Hiển thị toàn bộ thông tin của tất cả môn học
SELECT * FROM subjects;

--2 Hiển thị mã môn, tên môn, và số tín chỉ của 3 sinh viên 
SELECT subject_code, subject_name, credit FROM grades AS g INNER JOIN subjects AS sj ON sj.id = g.subject_id INNER JOIN students AS s ON s.id = g.student_id WHERE s.student_code IN ('20071750', '20102345', '20071850');

--3 Hiển thị toàn bộ thông tin 4 môn có số tín chỉ thấp nhất
SELECT * FROM subjects AS sj ORDER BY sj.credit ASC LIMIT 4;

--4 Hiển thị thông tin của sinh viên có mã sinh viên là: 20101234
SELECT * FROM students AS s WHERE s.student_code = '20101234';  

--5 Hiển thị toàn bộ danh sách điểm của môn MAT101 Các cột: Mã môn, tên môn, tên sinh viên, điểm thi, ngày thi
SELECT sj.subject_code, sj.subject_name, CONCAT(s.first_name, " ", s.last_name) AS fullname, g.grade, g.exam_date FROM students AS s INNER JOIN grades AS g ON s.id = g.student_id INNER JOIN subjects AS sj ON sj.id = g.subject_id WHERE sj.subject_code IN ('MAT101');

--6 Lấy ra danh sách các môn có ngày thi là ngày 2021-05-04
SELECT * FROM subjects AS sj INNER JOIN grades AS g ON sj.id = g.subject_id WHERE g.exam_date IN ('2021-05-04');

--7 Lấy ra thông tin 3 điểm thi thấp nhất: môn học, tên sinh viên, điểm thi, ngày thi
SELECT sj.subject_name, CONCAT(s.first_name, " ", s.last_name) AS fullname, g.grade, g.exam_date FROM grades AS g INNER JOIN subjects AS sj ON g.subject_id = sj.id INNER JOIN students AS s ON g.student_id = s.id ORDER BY g.grade ASC LIMIT 3;

--8 Lấy ra thông tin 1 sinh viên có điểm cao nhất của môn ECO101 Nếu có nhiều sinh viên có điểm bằng nhau thì vẫn lấy 1 sinh viên
SELECT sj.subject_code, sj.subject_name, CONCAT(s.first_name, " ", s.last_name) AS fullname,g.grade, g.exam_date FROM students AS s INNER JOIN grades AS g ON s.id = g.student_id INNER JOIN subjects AS sj ON sj.id = g.subject_id WHERE sj.subject_code IN ('ECO101') ORDER BY g.grade DESC LIMIT 1;



